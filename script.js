function snapCrackle(maxValue) {

    let retornoConCat = '';
    for (let i = 1; i <= maxValue; i++) {

        if (((i % 2) !== 0) && ((i % 5) == 0)) {
            retornoConCat += 'SnapCrackle, ';
        } else if ((i % 2) !== 0) {
            retornoConCat += 'Snap, ';
        } else if ((i % 5) == 0) {
            retornoConCat += 'Crackle, ';
        } else {
            retornoConCat += i + ', ';
        }
    }


    console.log(retornoConCat);
}

function snapCracklePrime(maxValue) {

    let retornoConCat = '';
    for (let i = 1; i <= maxValue; i++) {

        let prime = false;
        let contPrime = 0;

        for (let j = 1; j <= i; j++) {
            if ((i % j) == 0) {
                contPrime++;
            }
        }

        if ((i > 1) && (contPrime === 2)) {
            prime = true;
        }

        if (prime) {
            if (((i % 2) !== 0) && ((i % 5) == 0)) {
                retornoConCat += 'SnapCracklePrime, ';
            } else if ((i % 2) !== 0) {
                retornoConCat += 'SnapPrime, ';
            } else if ((i % 5) == 0) {
                retornoConCat += 'CracklePrime, ';
            } else {
                retornoConCat += 'Prime, ';
            }
        } else {

            if (((i % 2) !== 0) && ((i % 5) == 0)) {
                retornoConCat += 'SnapCrackle, ';
            } else if ((i % 2) !== 0) {
                retornoConCat += 'Snap, ';
            } else if ((i % 5) == 0) {
                retornoConCat += 'Crackle, ';
            } else {
                retornoConCat += i + ', ';
            }
        }


    }


    console.log(retornoConCat);
}